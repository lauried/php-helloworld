<?php
/**
 * @var \SlightlyInteractive\DI\Container $container
 */

$container->set(\SlightlyInteractive\App\HelloWorldModel::class)
    ->argument('message', $_ENV['HELLO_WORLD_MESSAGE']);
