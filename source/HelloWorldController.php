<?php

namespace SlightlyInteractive\App;

use Jasny\HttpMessage\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class HelloWorldController implements RequestHandlerInterface
{
    /** @var HelloWorldModel */
    private $model;

    /**
     * @param HelloWorldModel $model
     */
    public function __construct(HelloWorldModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $params = [
            'message' => $this->model->getMessage(),
        ];
        extract($params);
        ob_start();
        include('../templates/hello-world.php');
        $content = ob_get_clean();

        $response = new Response();
        $response->getBody()->write($content);
        return $response;
    }
}
