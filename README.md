# Hello World

This is a standalone application to test the Slightly Interactive
DI and routing packages.

## Getting Started

1. Run the server:

       php serve.php

2. Browse to http://localhost:8000

## Configuration

The application's variables are read from the file `.env`, which is
copied automatically from `.env.example` when the server first
starts. To configure, edit the values in `.env`.
