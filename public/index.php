<?php

use Dotenv\Dotenv;
use Jasny\HttpMessage\ServerRequest;
use Jasny\HttpMessage\Emitter;
use SlightlyInteractive\DI\Container;
use SlightlyInteractive\DI\NotFoundException;
use SlightlyInteractive\Router\Router;

require_once("../vendor/autoload.php");

$dotenv = Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$container = new Container();
require_once("../config/config.php");

$router = new Router();
$router->setDI($container);
require_once("../config/routes.php");

$request = (new ServerRequest)->withGlobalEnvironment();
try {
    $response = $router->dispatch($request);
} catch (NotFoundException $exception) {
    header("HTTP/1.1 404 Not Found");
    print("404 Not Found");
    return;
} catch (\Exception $exception) {
    header("HTTP/1.1 500 Internal Server Error");
    print("500 Internal Server Error");
}

$emitter = new Emitter();
$emitter->emit($response);
